#ifndef SRHOOKVTABLE_H
#define SRHOOKVTABLE_H

#ifdef WIN32
#include <windows.h>
#ifdef _MSC_VER
#ifdef _WIN64
typedef __int64 ssize_t;
#else
typedef int ssize_t;
#endif
#endif
#else
#include <fstream>
#include <string>
#include <regex>
struct mempage_info{
	void *start_addr = nullptr;
	void *end_addr = nullptr;
	std::string prot;
	void *offset = nullptr;
	std::string dev;
	size_t inode = 0;
	std::string module;
};
mempage_info MempageQuery(void *Memory){
	const std::regex parse(R"(([0-9a-f]+)-([0-9a-f]+)[ \t]+([rwxps-]{4})[ \t]+([0-9a-f]+)[ \t]+(\d{2}:\d{2})[ \t]+(\d+)[ \t]+([^\n]*))", std::regex::icase);
	mempage_info result;
	auto maps = std::ifstream("/proc/self/maps");
	std::string str;
	while(std::getline(maps, str)){
		std::cmatch m;
		if (std::regex_match(str.c_str(), m, parse)){
			size_t start = static_cast<size_t>(std::stoull(m[1].str(), nullptr, 16));
			size_t end = static_cast<size_t>(std::stoull(m[2].str(), nullptr, 16));
			if (reinterpret_cast<size_t>(Memory) < start || reinterpret_cast<size_t>(Memory) > end)
				continue;
			result.start_addr = reinterpret_cast<void*>(start);
			result.end_addr = reinterpret_cast<void*>(end);
			result.prot = m[3].str();
			result.offset = reinterpret_cast<void*>(std::stoull(m[4].str(), nullptr, 16));
			result.dev = m[5].str();
			result.inode = static_cast<size_t>(std::stoull(m[6].str(), nullptr, 16));
			result.module = m[7].str();
			break;
		}
	}
	return result;
}
#endif

/**
 * @brief Класс для установки хуков на виртуальные методы
 */
template<class Obj>
class SRHookVtable{
public:
	/**
	 * @brief Конструктор
	 * @details В конструкторе сразу заменяется виртуальная таблица, методы заменяются позднее через метод hookMethod
	 * @param pObj Объект класса с таблицей виртуальных методов
	 * @param count Количество виртуальных методов у объекта
	 */
	SRHookVtable( Obj *pObj, const ssize_t count )
		: object( pObj ), original( *reinterpret_cast<size_t **>( pObj ) ), count( count ) {
		this->vtable = new size_t[count];
		memcpy(this->vtable, original, count * sizeof(size_t));
		*reinterpret_cast<size_t**>(object) = vtable;
	}
	/**
	 * @brief Конструктор с автоматическим определением размера виртуальной таблицы
	 * @details В конструкторе сразу заменяется виртуальная таблица, методы заменяются позднее через метод hookMethod
	 * @param pObj Объект класса с таблицей виртуальных методов
	 */
	SRHookVtable(Obj *pObj) : object(pObj), original(*reinterpret_cast<size_t**>(pObj)), count(detectVtableCount(pObj)){
		this->vtable = new size_t[count];
		memcpy(this->vtable, original, count * sizeof(size_t));
		*reinterpret_cast<size_t**>(object) = vtable;
	}
	/**
	 * @brief Деструктор
	 * @details В деструкторе восстанавливается оригинальная таблица виртуальных методов
	 */
	virtual ~SRHookVtable(){
		*reinterpret_cast<size_t**>(object) = original;
		delete[] vtable;
	}

	/**
	 * @brief Установка хука на виртуальный метод
	 * @param id Номер виртуального метода
	 * @param addr Адрес метода который будет вызываться вмето оригинального метода
	 * @return Успешность установки хука. Неуспех возможен только в случае не верного id
	 */
	template<typename T, class... Args> bool hookMethod( const ssize_t &id, T ( *func )( Args... ) ) {
		if (!isIdValid(id))
			return false;
		struct {
			decltype( func ) func;
		} _{func};
		vtable[id] = *reinterpret_cast<size_t *>( &_ );
		return true;
	}
	/**
	 * @brief Получение адреса виртуального метода из таблицы хука
	 * @param id Номер метода
	 * @return Адрес метода
	 */
	virtual size_t getMethod( const ssize_t &id ) {
		if (!isIdValid(id))
			return 0;
		return vtable[id];
	}
	/**
	 * @brief Получение адреса виртуального метода из оригинальной таблицы
	 * @param id Номер метода
	 * @return Адрес метода
	 */
	virtual size_t getOriginalMethod( const ssize_t &id ) {
		if (!isIdValid(id))
			return 0;
		return original[id];
	}

	/**
	 * @brief Восстанавливает оригинальный метод
	 * @param id Номер метода
	 */
	virtual void restoreMethod( const ssize_t &id ) {
		if (!isIdValid(id))
			return;
		vtable[id] = original[id];
	}
	/**
	 * @brief Восстанавливает все методы из оригинальной таблицы виртуальных методолв.
	 */
	virtual void restoreAll(){
		memcpy(vtable, original, count * sizeof(size_t));
	}

	/**
	 * @brief Возвращает оригинальный объект
	 * @return Оригинальный объект
	 */
	virtual Obj *getObject(){
		return object;
	}

	static size_t detectVtableCount(Obj *pObj){
		size_t result = 0;
		auto vtbl = *reinterpret_cast<size_t**>(pObj);
#ifdef WIN32
		MEMORY_BASIC_INFORMATION MBI;
		VirtualQuery(vtbl, &MBI, sizeof(MBI));
		size_t hiAddr = reinterpret_cast<size_t>(MBI.BaseAddress) + MBI.RegionSize;
#else
		size_t hiAddr = reinterpret_cast<size_t>(MempageQuery(vtbl).end_addr);
#endif
		for (size_t i = 0; i < hiAddr; ++i, ++result){
			if (vtbl[i] == 0)
				break;
			try {
#ifdef WIN32
			VirtualQuery(reinterpret_cast<void*>(vtbl[i]), &MBI, sizeof(MBI));
			if (!(MBI.Protect & 0x10) &&
				!(MBI.Protect & 0x20) &&
				!(MBI.Protect & 0x40) &&
				!(MBI.Protect & 0x80) )
				break;
#else
			if (MempageQuery(reinterpret_cast<void*>(vtbl[i])).prot[2] != 'x')
				break;
#endif
			} catch (...) {
				return result - 1;
			}
		}
		return result;
	}

protected:
	/// Виртуальная таблица хука
	size_t *vtable;
	/// Оригинальная виртуальная таблица
	size_t *original;
	/// Объект, в котором заменяется виртуальная таблица
	Obj *object;
	/// Количество виртуальных методов
	ssize_t count;

	/**
	 * @brief Проверяет валидность номера метода
	 * @param id Номер метода
	 * @return Валидность номера метода
	 */
	virtual bool isIdValid( const ssize_t &id ) {
		if (id < 0 || id > count)
			return false;
		return true;
	}
};

#endif // SRHOOKVTABLE_H

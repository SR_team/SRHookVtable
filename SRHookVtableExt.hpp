#ifndef SRHOOKVTABLEEXT_H
#define SRHOOKVTABLEEXT_H

#include "SRHookVtable.hpp"
#include <deque>

#ifndef WIN32
#include <sys/mman.h>
#endif

/**
 * @brief Класс для установки хуков на виртуальные методы
 */
template<class Obj> class SRHookVtableExt : public SRHookVtable<Obj> {
public:
	SRHookVtableExt(Obj *pObj, ssize_t count) : SRHookVtable<Obj>(pObj, count){}
	SRHookVtableExt(Obj *pObj) : SRHookVtable<Obj>(pObj){}
	/**
	 * @brief Деструктор
	 * @details В деструкторе восстанавливается оригинальная таблица виртуальных методов
	 */
	virtual ~SRHookVtableExt(){
		deleteAllAsmHooks();
	}

	/**
	 * @brief Установка хука на виртуальный метод
	 * @param id Номер виртуального метода
	 * @param object Объект класса с методом, который будет вызван вместо оригинального метода
	 * @param method Метод который будет вызываться вмето оригинального метода. ВНИМАНИЕ! this в данном случае не передается
	 * @return Успешность установки хука. Неуспех возможен только в случае не верного id
	 */
	template<class C, typename T, class... Args>
	bool hookMethod( const ssize_t &id, C *obj, T ( C::*method )( Args... ) ) {
		if ( !SRHookVtable<Obj>::isIdValid( id ) ) return false;
		// get relative address
		auto relAddr = [](size_t from, size_t to) { return to - (from + 5); };
		union value2bytes{
			size_t value;
			unsigned char bytes[sizeof (size_t)];
		};

		value2bytes ths;
		ths.value = reinterpret_cast<size_t>(obj);
		value2bytes addr;
		unsigned char *asmHook = nullptr;
		struct {
			decltype( method ) method;
		} _{method};
		auto pMethod = *reinterpret_cast<void **>( &_ );
		bool isVirtual = ( reinterpret_cast<size_t>( pMethod ) <
						   ( SRHookVtable<Obj>::count * sizeof( size_t ) ) );
		int jmpOffset = 10;

		if constexpr (sizeof(size_t) == 4){
			asmHook = static_cast<unsigned char *>( myalloc( ( isVirtual ? 17 : 10 ) ) );
			if ( !isVirtual ) {
				jmpOffset = 5;
				addr.value = relAddr( reinterpret_cast<size_t>( asmHook ) + 5,
									  reinterpret_cast<size_t>( pMethod ) );
			} else
				addr.value = reinterpret_cast<size_t>( pMethod );
			// mov ecx, obj
			asmHook[0] = 0xb9;
			asmHook[1] = ths.bytes[0];
			asmHook[2] = ths.bytes[1];
			asmHook[3] = ths.bytes[2];
			asmHook[4] = ths.bytes[3];
			if ( isVirtual ) {
				value2bytes vtbl;
				vtbl.value = *reinterpret_cast<size_t *>( obj );
				// mov eax, vtbl
				asmHook[5] = 0xb8;
				asmHook[6] = vtbl.bytes[0];
				asmHook[7] = vtbl.bytes[1];
				asmHook[8] = vtbl.bytes[2];
				asmHook[9] = vtbl.bytes[3];
			}
		} else {
			if ( isVirtual ) return false;
			asmHook = static_cast<unsigned char *>( myalloc( ( isVirtual ? 27 : 15 ) ) );
			if ( isVirtual ) {
				jmpOffset = 20;
				addr.value = reinterpret_cast<size_t>( pMethod );
			} else
				addr.value = relAddr( reinterpret_cast<size_t>( asmHook ) + 10,
									  reinterpret_cast<size_t>( pMethod ) );
			// mov rcx, obj
			asmHook[0] = 0x48;
			asmHook[1] = 0xb9;
			asmHook[2] = ths.bytes[0];
			asmHook[3] = ths.bytes[1];
			asmHook[4] = ths.bytes[2];
			asmHook[5] = ths.bytes[3];
			asmHook[6] = ths.bytes[4];
			asmHook[7] = ths.bytes[5];
			asmHook[8] = ths.bytes[6];
			asmHook[9] = ths.bytes[7];
			if ( isVirtual ) {
				value2bytes vtbl;
				vtbl.value = *reinterpret_cast<size_t *>( obj );
				// mov rax, vtbl
				asmHook[10] = 0x48;
				asmHook[11] = 0xb8;
				asmHook[12] = vtbl.bytes[0];
				asmHook[13] = vtbl.bytes[1];
				asmHook[14] = vtbl.bytes[2];
				asmHook[15] = vtbl.bytes[3];
				asmHook[16] = vtbl.bytes[4];
				asmHook[17] = vtbl.bytes[5];
				asmHook[18] = vtbl.bytes[6];
				asmHook[19] = vtbl.bytes[7];
			}
		}
		if ( isVirtual ) {
			// add eax, addr
			asmHook[jmpOffset + 0] = 0x05;
			asmHook[jmpOffset + 1] = addr.bytes[0];
			asmHook[jmpOffset + 2] = addr.bytes[1];
			asmHook[jmpOffset + 3] = addr.bytes[2];
			asmHook[jmpOffset + 4] = addr.bytes[3];
			// jmp [eax]
			asmHook[jmpOffset + 5] = 0xFF;
			asmHook[jmpOffset + 6] = 0x20;
		} else {
			// jmp method
			asmHook[jmpOffset + 0] = 0xe9;
			asmHook[jmpOffset + 1] = addr.bytes[0];
			asmHook[jmpOffset + 2] = addr.bytes[1];
			asmHook[jmpOffset + 3] = addr.bytes[2];
			asmHook[jmpOffset + 4] = addr.bytes[3];
		}

		typedef void(*fn_t)();
		SRHookVtable<Obj>::hookMethod( id, ( fn_t( asmHook ) ) );
		asmHooks.push_back(asmHook);

		return true;
	}
	/**
	 * @brief Установка хука на виртуальный метод
	 * @param orig_method Указатель на оригинальный виртуальный метод
	 * @param object Объект класса с методом, который будет вызван вместо оригинального метода
	 * @param method Метод который будет вызываться вмето оригинального метода. ВНИМАНИЕ! this в данном случае
	 * не передается
	 * @return Успешность установки хука. Неуспех возможен только в случае не верного id
	 */
	template<class C, typename T, class... Args>
	bool hookMethod( T ( Obj::*orig_method )( Args... ), C *obj, T ( C::*method )( Args... ) ) {
		struct {
			decltype( orig_method ) orig_method;
		} _{orig_method};
		auto id = *reinterpret_cast<size_t *>( &_ ) / sizeof( size_t );
		return hookMethod( id, obj, method );
	}
	/**
	 * @brief Установка хука на виртуальный метод
	 * @param orig_method Указатель на оригинальный виртуальный метод
	 * @param addr Адрес метода который будет вызываться вмето оригинального метода
	 * @return Успешность установки хука. Неуспех возможен только в случае не верного id
	 */
	template<typename T, class... Args>
	bool hookMethod( T ( Obj::*orig_method )( Args... ), T ( *func )( Args... ) ) {
		struct {
			decltype( orig_method ) orig_method;
		} _{orig_method};
		auto id = *reinterpret_cast<size_t *>( &_ ) / sizeof( size_t );
		return hookMethod( id, func );
	}
	/**
	 * @brief Установка хука на COM метод
	 * @param orig_method Указатель на оригинальный псевдо-виртуальный метод
	 * @param object Объект класса с методом, который будет вызван вместо оригинального метода
	 * @param method Метод который будет вызываться вмето оригинального метода.
	 * @return Успешность установки хука. Неуспех возможен только в случае не верного id
	 */
	template<class C, typename T, class... Args>
	bool hookComMethod( T ( __stdcall Obj::*orig_method )( Args... ), C *obj,
						T ( C::*method )( Obj *ths, Args... ) ) {
		struct {
			decltype( orig_method ) orig_method;
		} _{orig_method};
		auto id = *reinterpret_cast<size_t *>( &_ ) / sizeof( size_t );
		return hookMethod( id, obj, method );
	}
	/**
	 * @brief Установка хука на виртуальный метод
	 * @param orig_method Указатель на оригинальный виртуальный метод
	 * @param addr Адрес метода который будет вызываться вмето оригинального метода
	 * @return Успешность установки хука. Неуспех возможен только в случае не верного id
	 */
	template<typename T, class... Args>
	bool hookComMethod( T ( __stdcall Obj::*orig_method )( Args... ), T ( *func )( Obj *ths, Args... ) ) {
		struct {
			decltype( orig_method ) orig_method;
		} _{orig_method};
		auto id = *reinterpret_cast<size_t *>( &_ ) / sizeof( size_t );
		return hookMethod( id, func );
	}
	/**
	 * @brief Получение адреса виртуального метода из оригинальной таблицы
	 * @param orig_method Указатель на виртуальную функцию
	 * @return Адрес метода
	 */
	template<typename T, class... Args> size_t getOriginalMethod( T ( Obj::*orig_method )( Args... ) ) {
		struct {
			decltype( orig_method ) orig_method;
		} _{orig_method};
		const auto id = *reinterpret_cast<size_t *>( &_ ) / sizeof( size_t );
		return SRHookVtable<Obj>::getOriginalMethod( id );
	}
	/**
	 * @brief Получение адреса виртуального метода из оригинальной таблицы
	 * @param orig_method Указатель на виртуальную функцию
	 * @return Адрес метода
	 */
	template<typename T, class... Args>
	size_t getOriginalComMethod( T ( __stdcall Obj::*orig_method )( Args... ) ) {
		struct {
			decltype( orig_method ) orig_method;
		} _{orig_method};
		const auto id = *reinterpret_cast<size_t *>( &_ ) / sizeof( size_t );
		return SRHookVtable<Obj>::getOriginalMethod( id );
	}
	/**
	 * @brief Вызывает оригинальный метод
	 * @param id Номер метода
	 * @param args Аргументы метода
	 * @return Результат метода
	 */
	template<typename T, class... Args> T callOriginalMethod( const ssize_t &id, Args... args ) {
		struct crutch {
			typedef T ( Obj::*method_t )( Args... );
			method_t method;
		};
		struct {
			ssize_t id;
		} _{static_cast<ssize_t>( SRHookVtable<Obj>::getOriginalMethod( id ) )};
		auto method = reinterpret_cast<crutch *>( &_ )->method;
		return ( SRHookVtable<Obj>::object->*method )( args... );
	}
	/**
	 * @brief Вызывает оригинальный метод
	 * @param id Номер метода
	 * @param args Аргументы метода
	 * @return Результат метода
	 */
	template<typename T, class... Args>
	T callOriginalMethod( T ( Obj::*orig_method )( Args... ), Args... args ) {
		struct {
			decltype( orig_method ) orig_method;
		} _{orig_method};
		auto id = *reinterpret_cast<size_t *>( &_ ) / sizeof( size_t );
		return callOriginalMethod<T>( id, args... );
	}
	/**
	 * @brief Вызывает оригинальный COM-метод
	 * @param id Номер метода
	 * @param args Аргументы метода
	 * @return Результат метода
	 */
	template<typename T, class... Args>
	T callOriginalComMethod( T ( __stdcall Obj::*orig_method )( Args... ), Obj *self, Args... args ) {
		struct {
			decltype( orig_method ) orig_method;
		} _{orig_method};
		auto id = *reinterpret_cast<size_t *>( &_ ) / sizeof( size_t );
		return callOriginalMethod<T>( id, self, args... );
	}
	/**
	 * @brief Восстанавливает оригинальный метод
	 * @param id Номер метода
	 */
	virtual void restoreMethod( const ssize_t &id ) {
		if ( !SRHookVtable<Obj>::isIdValid( id ) ) return;
		auto deleteAsmHook = [this](size_t func){
			for(int i = 0; i < asmHooks.size(); ++i){
				if (reinterpret_cast<size_t>(asmHooks.at(i)) == func){
					delete asmHooks.at(i);
					asmHooks.erase(asmHooks.begin() + i);
				}
			}
		};
		deleteAsmHook( SRHookVtable<Obj>::vtable[id] );
		SRHookVtable<Obj>::restoreMethod( id );
	}
	/**
	 * @brief Восстанавливает все методы из оригинальной таблицы виртуальных методолв.
	 */
	virtual void restoreAll(){
		deleteAllAsmHooks();
		SRHookVtable<Obj>::restoreAll();
	}

protected:

	/**
	 * @brief Удаляет все хуки использующие asm
	 */
	void deleteAllAsmHooks(){
		for(int i = 0; i < asmHooks.size(); ++i)
			delete[] asmHooks.at(i);
		asmHooks.clear();
	}

	virtual void* myalloc(size_t size){
#ifdef WIN32
		auto Unprotect = [](size_t address, size_t size) // Allow execute code. By FYP
		{
			do
			{
				MEMORY_BASIC_INFORMATION mbi;
				if (!VirtualQuery(reinterpret_cast<PVOID>(address), &mbi, sizeof(mbi)))
					throw "virtual query error";
				if (size > mbi.RegionSize)
					size -= mbi.RegionSize;
				else
					size = 0;
				DWORD oldp;
				if (!VirtualProtect(mbi.BaseAddress, mbi.RegionSize, PAGE_EXECUTE_READWRITE, &oldp))
					throw "virtual protect error";
				if (reinterpret_cast<size_t>(mbi.BaseAddress) + mbi.RegionSize < address + size)
					address = reinterpret_cast<size_t>(mbi.BaseAddress) + mbi.RegionSize;
			} while (size);
		};
		auto addr = malloc(size);
		Unprotect(reinterpret_cast<size_t>(addr), size);
		return addr;
#else
		return mmap(NULL,
					size,
					PROT_READ | PROT_WRITE | PROT_EXEC,
					MAP_ANONYMOUS | MAP_PRIVATE,
					0,
					0);
#endif
	}

private:
	/// Список asm хуков
	std::deque<unsigned char *> asmHooks;
};

#endif // SRHOOKVTABLEEXT_H
